TALK=talk.ss
RACO=/Applications/Racket-8.3-CS/bin/raco

all: preview

compile:
	@${RACO} make -v ${TALK}

pict: compile
	@${RACO} pict ${TALK}

show: compile
	@${RACO} slideshow --widescreen ${TALK}

pdf: compile
	@${RACO} slideshow --widescreen --condense --pdf ${TALK}

