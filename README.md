Sounds good.

We can use this Zoom to link Ben in:
https://utah.zoom.us/j/96007649887?pwd=NDZMbTVScWlqK3g5Tk9DOVl5MUZoUT09

So in terms of logistics (and trying to think through the details a bit):

- Ganesh will connect his laptop to the AV system in the room and join the Zoom session.
- Ben will present via Zoom for his part.
- Ganesh will present in person for his part, with Taylor doing a demo (using Ganesh's laptop?)

Does that sound right?

Couple of other things:

- Class is on Monday, 9:40-10:30am
You will basically have the whole time. I will *very briefly* introduce you, i.e., name and research area... So please include additional details about yourself in your presentation.
(Just to emphasize: this is (just) a 50 minute class (not a "normal" 80 minute class).)

- The class is in CRC 210... (about a ten+ minute walk from MEB).
The building is also known as the Christensen Center. Here is a link to the building on the UofU map:
https://bit.ly/2kagOEp
Once you have found the building, you will need to find the room... If you enter from the doors directly across from the Architecture Building you need to go up the stairs to the second floor. The room is on the right (south side) as you walk up the stairs. Easiest way to find it is to turn right at the top of the stairs and look for "Spencer Fox Eccles Class Rooms" on the right. CRCC 210 is clearly marked.  (You enter into a small "lobby" and then go through another door to get into the room.)

- Are you still good with this title: "Reliable numerical design for ML via PL"

- I include the suggested presentation structure below.

- If you are interested in "recruiting" undergraduates to be involved in your research, please mention that. And, if you are interested in taking undergraduates it would be helpful if you can indicate what courses you require/prefer they have taken beforehand.

- Please send me a copy of your slides after class.

- The room has an HDMI connector for the projector.

Thanks!
Kobus

Suggested structure for presentation:
- Provide "general audience" style background and motivation for your broader
  research area.
- Describe one or two of your current projects/research efforts in more detail.
- Provide a few ideas on remaining big challenges in your area and/or ideas you
  might be interested to explore.
- Provide pointers to a workshop and/or conference in your area that might be
  accessible for an interested/motivated undergrad to explore.

